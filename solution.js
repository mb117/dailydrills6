const profileDetails = require("./5-arrays-people-friends.js");

// ---Chain everything----.

// 1. Make the balance into a number. If the number is invalid, default to 0. Do not overwrite the existing key, create a new one instead.

// helper function to format currency string to number
function formatCurrencyString(currency) {
    // remove all $ and - sign
    currency = currency.replaceAll("$","")
    .replaceAll(",","");

    return isNaN(Number(currency)) ? 0 : Number(currency);
}

function convertBalanceToNumber(profileDetails) {
    profileDetails = profileDetails.map((user) => {
        const number = formatCurrencyString(user.balance);
        user["formattedBalance"] = number;

        return user;
    });

    return profileDetails;
}

// console.log(convertBalanceToNumber(profileDetails));

// 2. Sort by descending order of age.
function sortByAge(profileDetails) {
    profileDetails = profileDetails.sort((a,b) => {
        return b.age - a.age;
    });

    return profileDetails;
}

// console.log(sortByAge(profileDetails));

// 3. Flatten the friends key into a basic array of names.
function flattenFriendsKey(profileDetails) {
    profileDetails.map((user) => {
        const friends = user["friends"].map((friend) => {
            return friend["name"];
        });
        
        return friends;
    })
    .map((friends, index) => {
        profileDetails[index]["friends"] = friends;

        return friends;
    });

    return profileDetails;
}

console.log(flattenFriendsKey(profileDetails));
// console.log(profileDetails);



// 4. Remove the tags key.
function deleteTags(profileDetails) {
    profileDetails = profileDetails.map((user) => {
        delete user["tags"];

        return user;
    });

    return profileDetails;
}

// console.log(deleteTags(profileDetails));

// 5. Make the registered date into the DD/MM/YYYY format.

// helper function to formate dates into DD/MM/YYYY
function formatDate(date) {
    //YYYY-MM-DD
    date = date.split("T")[0]
    .split("-");

    return date[2] + "-" + date[1] + "-" + date[0];
}

function addFormattedDates(profileDetails) {
    profileDetails = profileDetails.map((user) => {
        user["registered"] = formatDate(user["registered"]);

        return user;
    });

    return profileDetails;
}

// console.log(addFormattedDates(profileDetails));


// 6. Filter only the active users.
function getActiveUsers(profileDetails) {
    profileDetails = profileDetails.filter((user) => {
        return user["isActive"]
    });

    return profileDetails;
}

// console.log(getActiveUsers(profileDetails));

// 7. Calculate the total balance available.
function getTotalBalance(profileDetails) {
    const totalBalance = convertBalanceToNumber(profileDetails)
    .reduce((totalBalance, user) => {
        return totalBalance + user["formattedBalance"];
    },0);

    return totalBalance;
}

// console.log(getTotalBalance(profileDetails));

